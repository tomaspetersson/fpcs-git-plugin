package se.fpcs;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Ref;

/**
 * 
 */
@Mojo(name = "tag-map", defaultPhase = LifecyclePhase.PROCESS_RESOURCES)
public class GitTagMapMojo extends AbstractMojo {

	private static final String REFS_TAGS = "refs/tags/";

	@Parameter(readonly = true, defaultValue = "${project}")
	private MavenProject project;

	@Parameter
	private String repoUri;

	/**
	 * 
	 */
	public void execute() throws MojoExecutionException {

		try {
			getLog().error(String.format("Git repository URI: %s", this.repoUri));

			Map<String, Ref> gitMap = Git.lsRemoteRepository().setRemote(this.repoUri).callAsMap();
			Map<String, String> tag2commitMap = new HashMap<String, String>();
			Map<String, String> commit2tagMap = new HashMap<String, String>();

			for (String key : gitMap.keySet()) {
				if (!key.startsWith(REFS_TAGS)) {
					continue;
				}
				final String tag = key.substring(REFS_TAGS.length());
				final String commit = gitMap.get(key).getObjectId().name();
				tag2commitMap.put(tag, commit);
				commit2tagMap.put(commit, tag);
			}

			writeToFile(tag2commitMap, "tag2commitMap");
			writeToFile(commit2tagMap, "commit2tagMap");

		} catch (Exception e) {
			throw new MojoExecutionException(
					String.format("execute() Exception: %s Message: %s", e.getClass().getName(), e.getMessage()), e);
		}

	}

	/**
	 * 
	 * @param map
	 * @param name
	 * @throws MojoFailureException
	 */
	private void writeToFile(Map<String, String> map, String name) throws MojoFailureException {

		File file = null;
		try {
			File folder = new File(project.getModel().getBuild().getDirectory(), "classes");
			file = new File(folder, name + ".properties");

			StringBuffer data = new StringBuffer("# ").append(file.getName()).append('\n') //
					.append("# ").append(new Date()).append('\n');
			for (String key : map.keySet()) {
				data.append(key).append(": ").append(map.get(key)).append('\n');
			}

			FileUtils.fileWrite(file, data.toString());
			getLog().info(String.format("Wrote resource file: %s", file.getAbsolutePath()));
		} catch (Exception e) {
			throw new MojoFailureException(
					String.format("Failed to write file: %s", (file == null ? "NULL" : file.getAbsolutePath())));
		}

	}

}
